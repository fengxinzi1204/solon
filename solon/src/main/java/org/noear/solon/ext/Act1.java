package org.noear.solon.ext;

public interface Act1<T> {
    void run(T t);
}