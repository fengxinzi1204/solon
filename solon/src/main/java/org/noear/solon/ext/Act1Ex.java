package org.noear.solon.ext;

public interface Act1Ex<T> {
    void run(T t) throws Exception;
}