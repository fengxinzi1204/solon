package org.noear.solon.ext;

public interface Act2<T1,T2> {
    void run(T1 t1,T2 t2);
}