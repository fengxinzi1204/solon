package org.noear.solon.extend.sessionstate.redis;

import org.noear.solon.XApp;
import org.noear.solon.XUtil;
import org.noear.solon.core.XHandlerLink;
import org.noear.solon.core.XMethod;
import org.noear.solon.core.XPlugin;
import org.noear.solon.core.XSessionStateDefault;

public class XPluginImp implements XPlugin {
    @Override
    public void start(XApp app) {
        /*
        *
        * server.session.state.redis:
        * server: redis.dev.zmapi.cn:6379
        * password: AVsVSKd1
        * db: 31
        * maxTotaol: 200
        *
        * */
        XServerProp.init();

        SessionState sessionState = new SessionState();

        XSessionStateDefault.global = sessionState;

        app.before("**", XMethod.HTTP,(c)->{
            sessionState.updateSessionID();
        });

        System.out.println("solon:: Redis session state is loaded");
    }
}
