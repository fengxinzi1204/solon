

#### 1.0.3.32::
* 1.添加定时任务支持
* 2.Xbean注册时，默认用className做为name

##### 1.0.3.24::
* 1.XPlugin 添加 defautl stop()接口
* 2.将插件stop 从Closeable 接口 改为 stop()

##### 1.0.3.22::
* 1.snack3升为：3.1.3
* 2.meven改为parent继承模式，所有版本与parent统一

#### 1.0.3.18::
* 1.XSessionState 添加 sessionClear()