package webapp.demo6_aop.dao;

public interface Rockapi {

    String test1(Integer a);

    Object test2();

    Object test3();

    Object test4();

    Object test5();

    Object test6();

    Object test7();

    Object test8();
}