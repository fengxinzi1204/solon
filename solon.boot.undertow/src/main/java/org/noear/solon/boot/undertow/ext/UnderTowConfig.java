package org.noear.solon.boot.undertow.ext;

import org.noear.solon.XApp;

/**
 * @Created by: Yukai
 * @Date: 2019/3/24 18:13
 * @Description : Yukai is so handsome xxD
 * 配合servlet使用
 */
public class UnderTowConfig {
    public static final String SESSION_COOKIE_NAME = "YKSession";
    public static XApp app;
    public static boolean debug;
}
