package org.noear.solonclient;

public interface HttpUpstream {
    String getTarget(String name);
}
