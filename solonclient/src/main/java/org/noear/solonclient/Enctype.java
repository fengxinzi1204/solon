package org.noear.solonclient;

/**
 * body encode type
 * */
public enum  Enctype {
    form_data,
    application_json;
}
